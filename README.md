# JWT Secret Key Generator

Usage: `java -jar dist/jwtsecretkeygen.jar <algorithm>`

Supported algorithms:
`PS384`,
`RS384`,
`ES384`,
`HS256`,
`HS512`,
`ES256`,
`RS256`,
`HS384`,
`ES512`,
`PS256`,
`PS512`,
`RS512`