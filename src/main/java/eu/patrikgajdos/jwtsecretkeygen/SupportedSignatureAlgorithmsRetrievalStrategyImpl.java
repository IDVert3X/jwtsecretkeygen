package eu.patrikgajdos.jwtsecretkeygen;

import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class SupportedSignatureAlgorithmsRetrievalStrategyImpl
        implements SupportedSignatureAlgorithmsRetrievalStrategy
{
    private static final Class<SignatureAlgorithm> SA_CLASS = SignatureAlgorithm.class;

    private final Map<String, SignatureAlgorithm> algorithmsMap;

    public SupportedSignatureAlgorithmsRetrievalStrategyImpl()
    {
        this.algorithmsMap = Arrays.stream(SA_CLASS.getEnumConstants())
                .filter((sa) -> !sa.name().equals("NONE"))
                .collect(Collectors.toMap(SignatureAlgorithm::name, (sa) -> sa));
    }

    @Override
    public Map<String, SignatureAlgorithm> getSupportedAlgorithms()
    {
        return this.algorithmsMap;
    }
}
