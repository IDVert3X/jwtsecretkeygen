package eu.patrikgajdos.jwtsecretkeygen;

import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Collection;
import java.util.Map;

public class Application
{
    private static JwtGenerationStrategy jwtGenerationStrategy;
    private static SupportedSignatureAlgorithmsRetrievalStrategy algoRetrievalStrategy;

    public static void main(String[] args)
    {
        initBeans();
        Map<String, SignatureAlgorithm> supportedAlgorithms = algoRetrievalStrategy.getSupportedAlgorithms();

        if (args.length != 1) {
            System.err.println("Expected exactly 1 parameter - signature algorithm name");
            failWithSupportedAlgorithms(supportedAlgorithms.keySet());
        }

        var algorithm = supportedAlgorithms.get(args[0]);
        if (algorithm == null) {
            System.err.println("Unsupported signature algorithm");
            failWithSupportedAlgorithms(supportedAlgorithms.keySet());
        }

        System.out.println(jwtGenerationStrategy.generateJwtKey(algorithm));
    }

    private static void failWithSupportedAlgorithms(Collection<String> supportedAlgorithms)
    {
        System.out.println("Supported algorithms: " + String.join(", ", supportedAlgorithms));
        System.exit(1);
    }

    private static void initBeans()
    {
        jwtGenerationStrategy = new JwtGenerationStrategyImpl();
        algoRetrievalStrategy = new SupportedSignatureAlgorithmsRetrievalStrategyImpl();
    }
}
