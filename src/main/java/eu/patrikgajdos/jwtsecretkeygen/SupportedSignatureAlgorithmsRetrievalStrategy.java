package eu.patrikgajdos.jwtsecretkeygen;

import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Map;

public interface SupportedSignatureAlgorithmsRetrievalStrategy
{
    /**
     * @return map of supported algorithm names to {@link SignatureAlgorithm}
     */
    Map<String, SignatureAlgorithm> getSupportedAlgorithms();
}
