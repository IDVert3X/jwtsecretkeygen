package eu.patrikgajdos.jwtsecretkeygen;

import io.jsonwebtoken.SignatureAlgorithm;

public interface JwtGenerationStrategy
{
    /**
     * Generate JWT secret key for given algorithm and output it as string
     *
     * @param algorithm Algorithm to generate the PK for
     * @return Generated private key as string
     */
    String generateJwtKey(SignatureAlgorithm algorithm);
}
