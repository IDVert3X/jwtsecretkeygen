package eu.patrikgajdos.jwtsecretkeygen;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Encoders;
import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;

public class JwtGenerationStrategyImpl implements JwtGenerationStrategy
{
    @Override
    public String generateJwtKey(SignatureAlgorithm algorithm)
    {
        SecretKey secret = Keys.secretKeyFor(algorithm);
        return Encoders.BASE64.encode(secret.getEncoded());
    }
}
